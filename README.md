# downstream-dtbs-src 

This contains all the source files needed (.dts, .dtsi, and .h) to create 
device-tree binaries for the downstream dtbs.

The RPM can be found at: 
https://gitlab.com/lkarpins-rh/downstream-dtbs
